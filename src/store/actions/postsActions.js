import http from '../../services/http';

export const DATA_STARTED = 'DATA_STARTED';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_FAILED = 'DATA_FAILED';

export const getPosts = () => (dispatch) => {

  dispatch({ type: DATA_STARTED, isLoading: true });
  http.get('/posts')
    .then(res => res.data)
    .then((data) => {
      dispatch({ type: DATA_SUCCESS, isLoading: false, payload: data });
    })
    .catch(err => {
      dispatch({ type: DATA_FAILED, hasError: true });
      console.error(err);
    });
}

export const deletePost = (id) => (dispatch) => {

  dispatch({ type: DATA_STARTED });
  http.delete(`/posts/${id}`)
    .then(res => {
      dispatch({ type: DATA_SUCCESS });
      console.log(`Post ${id} is deleted successfully`);
      dispatch(getPosts());
    })
    .catch(err => {
      dispatch({ type: DATA_FAILED });
      console.error(err);
    })
}

export const createPost = (data) => (dispatch) => {

  dispatch({ type: DATA_STARTED });
  http.post('/posts', data)
    .then(res => {
      dispatch({ type: DATA_SUCCESS });
      console.log('Post was created', res.data);
      dispatch(getPosts());
    })
    .catch(err => {
      dispatch({ type: DATA_FAILED });
      console.error(err);
    })
}