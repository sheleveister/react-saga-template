export const ADD_TRACK = 'ADD_TRACK';
export const REMOVE_TRACK = 'REMOVE_TRACK';
export const SEARCH_LIST_FILTER = 'SEARCH_LIST_FILTER';
export const GET_TRACK_LIST = 'GET_TRACK_LIST';

const mockDataTracksApi = {
  tracks: [
    { id: 0, value: 'test' },
    { id: 1, value: 'sdfhfkldf' },
    { id: 2, value: 'sdjfhfkldf' },
    { id: 3, value: 'jkdkjdfkj' },
    { id: 4, value: 'ioerioier' },
  ]
};

export const getTracks = () => (dispatch, getState) => {
  setTimeout(() => {
    // console.log('I got data');
    dispatch({ type: GET_TRACK_LIST, payload: mockDataTracksApi });

    // const res = getState();
    // console.log(res.tracksReducer.tracks);
  }, 2000);
}
