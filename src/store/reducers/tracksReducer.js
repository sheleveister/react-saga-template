import * as trackActions from '../actions/trackActions';

const initialState = {};

export default function tracksReducer(state = initialState, action) {
  switch (action.type) {
    case trackActions.GET_TRACK_LIST:
      // console.log('GET_TRACK_LIST', action.payload);
      return {
        ...action.payload
      }

    case trackActions.ADD_TRACK:
      return {
        tracks: [
          ...state.tracks,
          action.payload
        ]
      }

    case trackActions.REMOVE_TRACK:
      return {
        tracks: [...state.tracks].filter((item) => item.id !== action.payload)
      }

    case trackActions.SEARCH_LIST_FILTER:
      if (action.payload) {
        return {
          ...state,
          filteredTracks: foundTracks(state.tracks, action.payload)
        }
      } else {
        delete state.filteredTracks;
        return {
          tracks: [...state.tracks]
        };
      }

    default:
      return state;
  }
}

const foundTracks = (tracks, term) => {
  console.log(tracks);

  const result = tracks ? tracks.filter((track) => {
    return track.value.includes(term);
  }) : tracks;
  return result;
}