import * as postsActions from '../actions/postsActions';

export default function postsReducer(state = {}, action) {
  switch(action.type) {
    case postsActions.DATA_STARTED:
      return {
        ...state,
        loading: action.isLoading
      }

    case postsActions.DATA_SUCCESS:
      return {
        ...state,
        loading: action.isLoading,
        posts: action.payload,
      }

    default: 
      return state;
  }
}
