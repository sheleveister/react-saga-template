import { combineReducers } from 'redux';

import tracksReducer from './tracksReducer';
import postsReducer from './postsReducer';

export default combineReducers({
  tracksReducer,
  postsReducer,
});
