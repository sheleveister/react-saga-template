import './App.css';

import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Tracks from './components/Tracks/Tracks';
import Albums from './components/Albums/Albums';
// import Posts from './components/Posts/Posts';
import Nav from './components/Nav/Nav';

export default class App extends Component {

  render() {
    return (
      <div className="App">
        <Nav />

        {/* <Route exact path="/" component={Tracks} /> */}
        {/* <Route exact path="/posts" component={Posts} /> */}
        <Route exact path="/" render={() => <h1>Tracks</h1>} />
        <Route exact path="/albums" component={Albums} />
      </div>
    );
  }
}
