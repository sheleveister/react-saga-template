import './Nav.css';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class Nav extends Component {
  render() {
    return (
      <div className="Nav">
        <Link to="/">
          <span className="NavTitle">Tracks</span>
        </Link>

        <Link to="/posts">
          <span className="NavTitle">Posts</span>
        </Link>
        
        <Link to="/albums">
          <span className="NavTitle">Albums</span>
        </Link>

      </div>
    )
  }
}
