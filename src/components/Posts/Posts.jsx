import './Posts.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPosts, deletePost, createPost } from '../../store/actions/postsActions';
import Card from '../Card/Card';

class Posts extends Component {

  removePost = (postId) => {
    this.props.deletePost(postId);
  }

  createPost = () => {
    const data = {
      title: 'foo',
      body: 'bar',
      userId: 1
    };
    this.props.createPost(data);
  }

  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    const { posts } = this.props;
    const { loading } = this.props;

    if (!posts) {
      return null;
    }

    if (loading) {
      return <span> ...loading </span>;
    }

    return (
      <div className="Container">
        <div className="PostCard">
          <input type="text" placeholder="fake text input" className="input" />

          <button className="PostCardCreateBtn PostCardCreate"
            onClick={this.createPost}>
            Create item
        </button>
        </div>

        {posts.map(({ id, title, body }) => (
          <Card key={id}
            title={title}
            body={body}
            onclick={() => this.removePost(id)}
          />
        ))}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  posts: state.postsReducer.posts,
  loading: state.postsReducer.loading
});

const mapDispatchToProps = (dispatch) => ({
  getPosts: () => dispatch(getPosts()),
  deletePost: (id) => dispatch(deletePost(id)),
  createPost: (data) => dispatch(createPost(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Posts);