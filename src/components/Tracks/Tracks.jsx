import './Tracks.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as trackActions from '../../store/actions/trackActions';
import { getTracks } from '../../store/actions/trackActions';

class Tracks extends Component {

  state = {
    value: '',
    searchValue: ''
  };

  addTrack = (event) => {
    if (event.keyCode === 13) {
      const trackValue = {
        id: this.props.trackList.length,
        value: event.target.value
      };

      this.setState({ value: '' });
      this.props.addTrackToList(trackValue);
    }

  };

  handleToRemoveItem = (event, itemId) => {
    this.props.removeTrackFromList(itemId);
  };

  handleValueField = (event) => {
    this.setState({ value: event.target.value });
  };

  handleSearch = (event) => {
    this.props.trackSearch(event.target.value);
    this.setState({ searchValue: event.target.value });
  };

  componentDidMount() {
    this.props.onGetTracks();
  }

  render() {
    const { trackList, filteredTracks } = this.props;
    let itemsList = [];

    if (!this.state.searchValue) {
      itemsList = trackList;
    } else {
      itemsList = filteredTracks;
    }

    return (
      <div className="Tracks">

        <div className="searchPanel">
          <input className="input"
            type="search"
            placeholder="Search track"
            onKeyUp={this.handleSearch}
          />
        </div>

        <div className="control">
          <input className="input"
            value={this.state.value}
            type="text"
            placeholder="Add track"
            onChange={this.handleValueField}
            onKeyUp={this.addTrack}
          />
        </div>

        <div className="list">
          {itemsList ? itemsList.map((item) => (
            <span className="" key={item.id}
              onClick={(e) => this.handleToRemoveItem(e, item.id)}>
              <b> {item.id + 1} </b>
              {item.value}
            </span>
          )) : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  trackList: state.tracksReducer.tracks,
  filteredTracks: state.tracksReducer.filteredTracks
});

const mapDispatchToProps = (dispatch) => ({
  onGetTracks: () => dispatch(getTracks()),
  addTrackToList: (trackValue) => dispatch({ type: trackActions.ADD_TRACK, payload: trackValue }),
  removeTrackFromList: (trackId) => dispatch({ type: trackActions.REMOVE_TRACK, payload: trackId }),
  trackSearch: (searchValue) => dispatch({ type: trackActions.SEARCH_LIST_FILTER, payload: searchValue })
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);
