import './Card.css';
import React, { Component } from 'react';

export default class Card extends Component {
  render() {
    const { id, title, body, onclick } = this.props;

    return (
      <div className="PostCard" key={id}>
        <span className="PostCardTitle">{title}</span>
        <span className="PostCardDescription">{body}</span>

        <div className="BtnGroup">
          <button className="PostCardBtn PostCardDelete"
            onClick={onclick}>Delete card</button>
        </div>
      </div>
    )
  }
}