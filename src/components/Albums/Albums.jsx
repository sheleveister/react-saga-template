import './Albums.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from '../Card/Card';
import { requestFetchUsersAction } from '../../saga-store/modules/albums/albums.actions';

class Albums extends Component {

  componentDidMount() {
    this.props.getAlbums();
  }

  render() {
    const { albums } = this.props.albums;
    console.log(albums);

    if (!albums) {
      return null;
    }

    return (
      <div className="Container">
        {albums.map(({ id, title }) => (
          <Card key={id}
            title={title}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  albums: state.albums,
});

const mapDispatchToProps = (dispatch) => ({
  getAlbums: () => dispatch(requestFetchUsersAction()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Albums);
