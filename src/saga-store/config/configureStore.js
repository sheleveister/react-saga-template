import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducer from '../reducer';
import rootSaga from '../modules/root/root.saga';

const sagaMiddleware = createSagaMiddleware();

export default function(state) {
  const store = createStore(reducer, compose(applyMiddleware(sagaMiddleware)));
  sagaMiddleware.run(rootSaga);

  if (module['hot']) {
    module['hot'].accept('../reducer', () => {
      return store.replaceReducer(require('../reducer').default);
    })
  } 
  return store;
}
