import { combineReducers } from 'redux';
import albums from './modules/albums/albums.reducer';

export default combineReducers({
  albums,
});
