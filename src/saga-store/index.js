import configStore from './config/configureStore';

const configureStore = configStore;

export default configureStore({});
