import { all } from 'redux-saga/effects';
import watchAlbums from '../albums/albums.saga';

export default function* rootSaga() {
  yield all([
    watchAlbums(),
  ]);
}
