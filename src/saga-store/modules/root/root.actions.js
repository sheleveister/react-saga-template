export const SERVER_ERROR = 'SERVER_ERROR';

export const setServerError = (error) => ({
  type: SERVER_ERROR,
  payload: {
    error,
  },
});
