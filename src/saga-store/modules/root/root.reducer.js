import { SERVER_ERROR } from './root.actions';

const initialState = {
  serverError: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SERVER_ERROR: 
      return {
        ...state,
        serverError: action.payload.error
      };
    default: 
      return state;
  }
}
