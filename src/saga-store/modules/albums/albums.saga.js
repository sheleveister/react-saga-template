import { call, put, takeLatest, all } from 'redux-saga/effects';
import { getAlbums } from './albums.api';
import { REQUEST_FETCH_ALBUMS, setAlbumsAction } from './albums.actions';
import { setServerError } from '../root/root.actions';

function* readAlbumsSaga(action) {
  try {
    const albums = yield call((() => getAlbums()));
    yield put(setAlbumsAction(albums));
  } catch (err) {
    yield put(setServerError('Server error, we are so sorry'));
  }
}

export default function* () {
  yield all([
    takeLatest(REQUEST_FETCH_ALBUMS, readAlbumsSaga)
  ])
}
