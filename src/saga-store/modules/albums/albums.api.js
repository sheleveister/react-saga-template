import http from '../../../services/http';

export const getAlbums = () => {
  return http.get('/albums')
    .then(res => res.data);
}
