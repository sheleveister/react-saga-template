export const REQUEST_FETCH_ALBUMS = 'REQUEST_FETCH_ALBUMS';
export const SET_ALBUMS = 'SET_ALBUMS';

// action creators
export const requestFetchUsersAction = () => ({
  type: REQUEST_FETCH_ALBUMS
});

export const setAlbumsAction = (data) => ({
  type: SET_ALBUMS,
  payload: { data }
});
