import { REQUEST_FETCH_ALBUMS, SET_ALBUMS } from './albums.actions';

export default function (state = {}, action) {
  switch (action.type) {
    case REQUEST_FETCH_ALBUMS:
      return { ...state, loading: true }
    
    case SET_ALBUMS:
      const { data } = action.payload;
      return { ...state, loading: false, albums: data }

    default: 
      return state;
  }
}